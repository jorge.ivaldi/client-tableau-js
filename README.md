# Client Tableau JS



## How to use
1- Add into your index.html head the required library:

```html
<!-- jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha512-3P8rXCuGJdNZOnUx/03c1jOTnMn3rP63nBip5gOP2qmUh5YAdVAvFZ1E+QLZZbC1rtMrQb+mah3AfYW11RUrWA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- Tableau api -->
<script type="text/javascript" src="https://public.tableau.com/javascripts/api/tableau-2.min.js"></script>

```

2- Add into your index.html head this library (verify tag version):
```html
<!-- Tableau Client Api JS Global Vars -->
<script src="https://cdn.statically.io/gl/jorge.ivaldi/client-tableau-js/stable-X.Y.Z/js/ts-repository.js"></script>
<!-- Tableau Client Api JS -->
<script src="https://cdn.statically.io/gl/jorge.ivaldi/client-tableau-js/stable-X.Y.Z/js/class.tableau.js"></script>
<!-- Tableau Client Api JS Global Styles -->
<link rel="stylesheet" type="text/css" href="https://cdn.statically.io/gl/jorge.ivaldi/client-tableau-js/stable-X.Y.Z/css/styles.css" />
```

3- Add this code into your index.html body
```html
<div id="vizContainer" class="col-12 ar-black-border hidde-input">
    <svg id="viz-container-loader" class="spinner" viewBox="0 0 50 50">
        <circle class="path" cx="25" cy="25" r="20" fill="none" stroke-width="5"></circle>
    </svg>
</div>
```