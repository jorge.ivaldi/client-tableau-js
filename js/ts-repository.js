if(!tableau) throw new Error("tableau var not exists")
var source_ts = tableau
var TS_ParameterDataType = source_ts?.ParameterDataType
var TS_FilterType = source_ts?.FilterUpdateType
var TS_EventNames = source_ts?.TableauEventName
var TS_ParameterAllowableValuesType = source_ts?.ParameterAllowableValuesType
var TS_Viz = source_ts?.Viz
