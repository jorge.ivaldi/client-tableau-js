class TSLogger {
    version = "1.0.2"
    static logger = console

    static error(cls, error) {
        throw new Error(`"${cls.name} [Error]: ${error.toString()}`)
    }

    static setLogger(logger) {
        TSLogger.logger = logger
    }

    static _log(...args) {
        if (TSLogger.logger.log) return TSLogger.logger.log(...args)
    }

    static _warn(...args) {
        if (TSLogger.logger.warn) return TSLogger.logger.warn(...args)
    }

    static _error(...args) {
        if (TSLogger.logger.error) return TSLogger.logger.error(...args)
    }

    static log(cls, message, { error: isError, warning: isWarning } = { error: false, warning: false }) {
        let type = isError ?
                        "Error" :
                        isWarning ?
                            "Warning" : "Log"
        let fn_log = isError ?
                        TSLogger._error :
                        isWarning ?
                            TSLogger._warn : TSLogger._log

        fn_log(TSLogger.classLog(cls, type), message);
    }

    static classLog(cls, type = "Log") {
        return `> ${cls.name} [${type}]:`
    }
}

class TSViz {
    viz = null
    _id = null
    url = ""
    version = "1.1.0"

    _states = ["none","initializated"]
    _state = this._states[0]
    visible = true
    _displayOnly = false

    _enableKeyboardEvents = true
    _enablePointerEvents = true

    devices = {
        desktop: "desktop",
        mobile: "phone"
    }

    _mobile_width = 1200

    container = null
    _eventsTypes = ["viz","container"]

    _TSVizEventsName = {} // names only
    _vizEvents = {} // bag events for viz

    _containerEventsName = {
        beforeResize: "before-resize",
        resize: "resize",
        afterResize: "after-resize"
    } // events name for container
    _containerEvents = {} // bug events for container

    _enum_debug = ["event-only","info","none"]
    _debug = this._enum_debug[0]

    _default_options = {
        hideToolbar: true,
        hideTabs: true,
    }

    _options = this._default_options

    _size = {
        width: 0,
        height: 0
    }

    current_worksheet = null

    get id() { return this._id }

    get debug() { return this._debug }

    get displayOnly() { return this._displayOnly }

    get isEnableKeyboardEvents() { return this._enableKeyboardEvents }
    get isEnablePointerEvents() { return this._enablePointerEvents }

    debugIsEnable() { return (this._enum_debug.indexOf(this._debug) > -1) }

    get state() { return this._state }

    get size() { return this._size }

    constructor({ id, container, visible = true, displayOnly = false, enableKeyboardEvents = true, enablePointerEvents = true, debug = "none", options = undefined, vizEvents = undefined, containerEvents = undefined, setInstanceOnWindowLoad = false, url = undefined }) {
        if(typeof(id) !== "string") TSLogger.error(TSViz, "id must be string")
        if(id.length === 0) TSLogger.error(TSViz, "id must be a valid string")

        // establezco id
        this._id = id

        this._debug = debug

        this._displayOnly = displayOnly

        this._enableKeyboardEvents = enableKeyboardEvents
        this._enablePointerEvents = enablePointerEvents

        // inidico estado inicial
        this._state = this._states[0]
        // establezco propiedad para inidicar si la instancia será cargada en el evento load de window
        this.setInstanceOnWindowLoad = setInstanceOnWindowLoad
        // si recibio opciones la seteo, sino les pongo valor por defecto
        this.options = options ?? this.defaultOptions
        // indico si esta visible o no
        this.visible = visible

        // preparo container
        this.prepareContainer(container)

        // preparo eventos asociados al container
        if(containerEvents) Object.entries(containerEvents).map(([name, event]) => {
            this.addContainerEventListener(name, event)
        })

        // si recibí url inicializo
        if(url) this.init({ url })

        // preparo eventos asociados al viz
        if(vizEvents) Object.entries(vizEvents).map(([name, event]) => {
            this.addVizEventListener(name, event)
        })

    }

    _set_viz_instance() {
        if(!this.container) TSLogger.error(TSViz, "vizContainer not loaded")

        // preparo viz instance
        this.viz = new TS_Viz(this.container, this.url, this._options)
        this._state = this._states[1]

        // muestro u oculto viz
        if(this.visible) this.show()
        else this.hide()
        this._addWindowResizeEventListener()
    }

    init({ url }) {
        // preparo url a usar
        this.prepareURL(url, { verify_device: true })

        // initializate viz instance
        if(this.setInstanceOnWindowLoad) window.addEventListener("load", this._set_viz_instance.bind(this))
        else this._set_viz_instance()
    }

    /**
    * Prepare URL, verifying devices or only set it
    * @param { string | { desktop: string, mobile: string } } url
    * @param { { verify_device?: bool | false } } options
    * @return { void }
    */
    prepareURL(url, { verify_device = false, default_device = 'desktop' } = {}) {
        if(!verify_device) {
            if(typeof(url) === 'string') this.url = url ?? ""
            else  this.url = url[default_device] ?? ""
            return this.url
        }

        if(typeof(url) === "string") this.url = url
        else {
            const device = screen.width >= this.mobileWidth ? 'desktop' : 'phone'
            this.url = url[device]
        }
    }

    prepareContainer(container = undefined) {
        this.container = container ?? document.getElementById("vizContainer")
        return this.container
    }

    /**
    * Define mobile width to TSViz
    * @param { number } new_width
    * @return { void }
    */
    set mobileWidth(new_width) {
        if(typeof(new_width) !== "number") TSLogger.error(TSViz, "new_width must be number")
        if(new_width === 0) return

        this._mobile_width = new_width
    }

    /**
    * get mobile width defined into TSViz
    * @return { number }
    */
    get mobileWidth() { return this._mobile_width }
    get containerEvents() { return Object.assign({}, this._containerEvents) }
    get containerEventsName() { return Object.assign({}, this._containerEventsName) }
    get TSVizEventsName() { return Object.assign({}, this._TSVizEventsName) }
    get vizEvents() { return Object.assign({}, this._vizEvents) }

    _cleanEventListener(type) {
        if(this._eventsTypes.indexOf(type) == -1) TSLogger.error(TSViz, `cleanEventListener type ${type} not found`)

        if(type === this._eventsTypes[0]) {
            // elimino callback definido
            this.viz && Object.entries(this._vizEvents).map(([name, callback]) => {
                this.viz.removeEventListener(name, callback)
            })
            // reseteo info
            this._vizEvents = {}
        }

        if(type === this._eventsTypes[1]) {
            // elimino callback definido
            this.container && Object.entries(this._containerEvents).map(([name, callback]) => {
                this.container.removeEventListener(name, callback)
            })
            // reseteo info
            this._containerEvents = {}
        }
    }

    cleanVizEventListeners() { this._cleanEventListener(this._eventsTypes[0]) }

    cleanContainerEventListeners() { this._cleanEventListener(this._eventsTypes[1]) }

    _addEventListener({name, callback, type}) {
        if(this._eventsTypes.indexOf(type) == -1) TSLogger.error(TSViz, `addEventListener type ${type} not found`)

        if(type === this._eventsTypes[0]) {
            if(
                Object.values(this.TSVizEventsName).concat(
                    Object.values(TS_EventNames)
                ).indexOf(name) == -1
            ) TSLogger.error(TSViz, `viz Event ${name} not found`)

            this._vizEvents[name] = callback
            this.viz && this.viz.addEventListener(name, this._vizEvents[name].bind(this))
        }

        if(type === this._eventsTypes[1]) {
            if(
                Object.values(this.containerEventsName).indexOf(name) == -1
            ) TSLogger.error(TSViz, `container Event ${name} not found`)

            this._containerEvents[name] = callback
            this.container && this.container.addEventListener(name, this._containerEvents[name].bind(this))
        }
    }

    refreshVizSize() {
        this.viz && this.viz.refreshSize()
    }

    show({ showOnCallbackOfResolve = false, callbackBeforeRefreshData = null } = {}) {
        return new Promise(async (resolve) => {
            if(this.viz && document.querySelectorAll('#vizContainer iframe').length) {
                let vizContainer = document.querySelector('#vizContainer')
                let iframe = document.querySelector('#vizContainer iframe')

                this.viz.show()
                this.viz.refreshSize()
                // console.log({ callbackBeforeRefreshData })
                if(typeof(callbackBeforeRefreshData) === 'function') callbackBeforeRefreshData()
                this.viz.refreshDataAsync()
                    .then(() => {
                        this.visible = true
                        if(!showOnCallbackOfResolve) {
                            iframe.classList.remove('hide')
                            vizContainer.classList.remove('hide')
                            iframe.style.opacity = '1'
                        }

                        resolve({
                            visible: this.visible,
                            show() {
                                if(showOnCallbackOfResolve) {
                                    iframe.classList.remove('hide')
                                    vizContainer.classList.remove('hide')
                                    iframe.style.opacity = '1'
                                }
                            }
                        })
                    })
                    .otherwise(err => {
                        if(!showOnCallbackOfResolve) {
                            iframe.classList.remove('hide')
                            vizContainer.classList.remove('hide')
                            iframe.style.opacity = '1'
                        }
                        reject({
                            ...err,
                            show() {
                                if(showOnCallbackOfResolve) {
                                    iframe.classList.remove('hide')
                                    vizContainer.classList.remove('hide')
                                    iframe.style.opacity = '1'
                                }
                            }
                        })
                    })
            } else {
                reject({ error: true, message: 'viz prop not set' })
            }
        })
    }

    hide({ hideOnCallbackOfResolve = false } = {}) {
        return new Promise(async (resolve) => {
            if(this.viz && document.querySelectorAll('#vizContainer iframe').length) {
                let vizContainer = document.querySelector('#vizContainer')
                let iframe = document.querySelector('#vizContainer iframe')

                this.viz.hide()
                this.viz.refreshDataAsync()
                .then(() => {
                    this.visible = false
                    if(!hideOnCallbackOfResolve) {
                        iframe.classList.add('hide')
                        vizContainer.classList.add('hide')
                        iframe.style.opacity = '0'
                    }

                    resolve({
                        visible: this.visible,
                        hide() {
                            if(hideOnCallbackOfResolve) {
                                iframe.classList.add('hide')
                                vizContainer.classList.add('hide')
                                iframe.style.opacity = '0'
                            }
                        }
                    })
                })
                .otherwise(err => {
                    if(!hideOnCallbackOfResolve) {
                        iframe.classList.add('hide')
                        vizContainer.classList.add('hide')
                        iframe.style.opacity = '0'
                    }

                    reject({
                        ...err,
                        hide() {
                            if(hideOnCallbackOfResolve) {
                                iframe.classList.add('hide')
                                vizContainer.classList.add('hide')
                                iframe.style.opacity = '0'
                            }
                        }
                    })
                })
            } else {
                reject({ error: true, message: 'viz prop not set' })
            }
        })
    }

    addVizEventListener(name, callback) {
        this._addEventListener({ type: this._eventsTypes[0], name, callback })
    }

    addContainerEventListener(name, callback) {
        this._addEventListener({ type: this._eventsTypes[1], name, callback })
    }

    _removeEventListener({type, name}) {
        if(this._eventsTypes.indexOf(type) == -1) TSLogger.error(TSViz, `removeEventListener type ${type} not found`)

        if(type === this._eventsTypes[0]) {
            if(!(name in this._vizEvents)) return

            this.viz && this.viz.removeEventListener(name, this._vizEvents[name])
            delete this._vizEvents[name]
        }

        if(type === this._eventsTypes[1]) {
            if(!(name in this._containerEvents)) return

            this.container && this.container.removeEventListener(name, this._containerEvents[name])
            delete this._containerEvents[name]
        }
    }

    removeVizEventListener(name) { this._removeEventListener({ type: this._eventsTypes[0], name }) }

    removeContainerEventListener(name) { this._removeEventListener({ type: this._eventsTypes[1], name }) }


    _setSize({ width, height, debug = (this.debugIsEnable() && this.debug == this._enum_debug[0]) }) {
        if(this.viz && this.visible) {
            this._size = { width, height }
            if(debug) TSLogger.log(TSViz, `update size on class -> width: ${width}, height: ${height}`);

            this.viz.setFrameSize(this.size.width, this.size.height);
            if(debug) TSLogger.log(TSViz, `update size del Vizcontainer -> width: ${width}, height: ${height}`);
        }
    }

    setSize({ width, height, debug = (this.debugIsEnable() && this.debug == this._enum_debug[0]) }) {
        this._setSize({ width, height, debug })
    }

    exportAsPDF() {
        this.viz && this.viz.showExportPDFDialog()
    }

    exportAsImage() {
        this.viz && this.viz.showExportImageDialog()
    }

    showExportDialog() {
        this.viz && this.viz.showDownloadDialog()
    }

    _onResize({ debug = (this.debugIsEnable() && this.debug == this._enum_debug[0]) } = {}) {
        // si el container y viz estan definidos entonces prosigo
        if(this.container && this.viz) {
            // despacho evento custom before-resize del container
            this.container.dispatchEvent(new CustomEvent("before-resize", {
                detail: {
                    size: this.size,
                    visible: this.visible
                }
            }));

            const { offsetWidth: width, offsetHeight: height } = this.container;
            const iframeContainer = this.container.querySelector("iframe");
            const { offsetWidth: iframeWidth, offsetHeight: iframeHeight } = iframeContainer

            if(debug) TSLogger.log(TSViz, `Event container.resize will be dispatched`);

            // despacho evento resize del container
            let update_size = this.container.dispatchEvent(new CustomEvent(this.containerEventsName.resize, {
                cancelable: true,
                detail: {
                    iframe: iframeContainer,
                    visible: this.visible,
                    size: {
                        current: this.size,
                        container: { width, height },
                        iframe: {
                            width: iframeWidth,
                            height: iframeHeight
                        }
                    }
                }
            }));

            // si no hay ni un prevent default en los eventos entonces actualizo el tamaño
            if(update_size) {
                this._setSize({ width, height })
                debug && this.viz && this.visible && TSLogger.log(TSViz, `iframe -> width: ${iframeWidth}, height: ${iframeHeight}`)

                // despacho evento custom after-resize del container
                this.container && this.container.dispatchEvent(new CustomEvent("after-resize", {
                    detail: {
                        size: {
                            current: this.size,
                            iframe: {
                                width: iframeWidth,
                                height: iframeHeight
                            }
                        }
                    }
                }));
            } else {
                debug && this.viz && TSLogger.log(TSViz, `Event container.resize prevent by a handler`);
            }
        }
    }

    _addWindowResizeEventListener({ runAfterAdd = true } = {}) {
        // agrego evento resize al viz, si este existe
        window.addEventListener("resize", this._onResize.bind(this))

        // ejecuto primera vez resize
        this._onResize()
    }

    trigger(eventName) {
        if(!["resize"].includes(eventName)) TSLogger.log(TSViz, `Event ${eventName} not found`);

        if(eventName === "resize") $(window).trigger('resize')
    }

    _removeWindowResizeEventListener() {
        // quito evento resize al viz, si este existe
        window.removeEventListener("resize",this._onResize)
    }

    // on destroy class
    destroy() {
        this.cleanVizEventListeners()
        this.cleanContainerEventListeners()
        this._removeWindowResizeEventListener()
    }

    setDisplayOnly(displayOnly, { save = true, apply = true } = {}) {
        if(!document.querySelectorAll('#vizContainer iframe').length && apply) return

        if(save) this._displayOnly = !displayOnly

        if(apply) {
            this.enableKeyboardEvents(false)
            this.enablePointerEvents(false)
        }
    }

    enableKeyboardEvents(enable, { save = true, apply = true } = {}) {
        if(!document.querySelectorAll('#vizContainer iframe').length && apply) return

        if(save) this._enableKeyboardEvents = !enable
        // agregandole tabindex -1, hago que no pueda hacer foco
        // quitandole tabindex, hago que pueda hacer foco
        if(apply) document.querySelector('#vizContainer iframe').setAttribute('tabindex', enable ? '' : '-1')
    }

    enablePointerEvents(enable, { save = true, apply = true } = {}) {
        if(!document.querySelectorAll('#vizContainer iframe').length && apply) return

        if(save) this._enablePointerEvents = !enable
        // con esta propiedad css le quito los eventos del cursor poniendolo en none
        if(apply) document.querySelector('#vizContainer iframe').style.pointerEvents = enable ? '' : 'none'
    }

    _onFirstInteractive(cb) {
        // get spinner and ts_view_container
        const [spinner, ts_view_container] = document.querySelectorAll('[id="viz-container-loader"],[id="ts_view_container"]')

        // si hay spinner lo oculto y si hay ts_view_container le pongo opacidad = 1
        if (spinner) spinner.classList.add('d-none');
        if (ts_view_container) ts_view_container.style.opacity = '1';

        // si ya esta cargado el iframe
        if(document.querySelectorAll('#vizContainer iframe').length) {
            // console.log({ displayOnly: this.displayOnly }) // debug
            // si es modo de solo mostrar, deshabilito eventos de teclado y mouse
            if(this.displayOnly) {
                this.setDisplayOnly(this.displayOnly, { save: false })
            } else {
                // le indico si esta deshabilitado eventos de teclado y mouse según como se hayan definido
                this.enableKeyboardEvents(this.isEnableKeyboardEvents, { save: false })
                this.enablePointerEvents(this.isEnablePointerEvents, { save: false })
            }
        }

        if(typeof(cb) === 'function') cb()
    }

    set options(opts) {
        let onFirstInteractive = null
        if("onFirstInteractive" in opts) {
            // guardo la funcion que tiene
            const onFirstInteractiveFromProps = opts.onFirstInteractive

            // genero nueva funcion con la funcion por defecto + la funcion recibida
            onFirstInteractive = (function () {
                // por si el metodo es asyncrono
                if(onFirstInteractiveFromProps.constructor.name === "AsyncFunction") {
                    // por si el metodo es asyncrono
                    new Promise(async (resolve,  reject) => {
                        const cb = await onFirstInteractiveFromProps(this)
                        resolve((() => {
                            this._onFirstInteractive(cb)
                        })())
                    })
                }

                if(onFirstInteractiveFromProps.constructor.name === "Function") {
                    onFirstInteractiveFromProps(this)
                }
            }).bind(this)

        } else {
            // genero funcion por defecto
            onFirstInteractive = (function () {
                this._onFirstInteractive()
            }).bind(this)
        }

        // re asigno funcion
        opts.onFirstInteractive = onFirstInteractive

        // asigna a _options el opts recibido
        // esto no es asignacion profunda !!
        Object.assign(this._options, opts)
    }

    // retorna opciones por default desvinculado
    get defaultOptions() { return Object.assign({}, this._default_options) }

    // retorna opciones como objeto desvinculado
    get options(){ return Object.assign({}, this._options) }

    getWorkbook() {
        if(!this.viz) return undefined

        return this.viz
                .getWorkbook()
    }

    getActiveSheet({ activateSheetAsync = false, worksheetName = undefined } = {}) {
        if(!this.viz) return undefined

        // si no se busca la hoja activa, entonces es necesario el parametro
        if(activateSheetAsync && worksheetName && typeof(worksheetName) !== "string") TSLogger.error(TSViz, "worksheetName param must be string")
        if(activateSheetAsync && worksheetName && worksheetName.length === 0) TSLogger.error(TSViz, "worksheetName param can not be empty")

        // si busca la activa, entonces la retorno
        if(!activateSheetAsync) return this.getWorkbook().getActiveSheet()

        // si busco sheet por nombre, entonces la busco y la activo
        return this.getWorkbook().activateSheetAsync(worksheetName)
    }

    applyParameter(parameter) {
        TSParameter.verify(parameter, { from: "viz" }) // verifico prameter

        const { name: parameterName, value: parameterValue } = parameter
        return new Promise((resolve, reject) => {
            this.getWorkbook().changeParameterValueAsync(parameterName, parameterValue)
            .then(() => {
                resolve({ updated: true, name: parameterName, value: parameterValue })
            })
            .otherwise((err) => reject({ name: parameterName, value: parameterValue, error: err }));
        })
    }

    async getWorkBookAllowableValuesOfParameterByParameterName(parameter) {
        if(!this.viz) return undefined
        const params = await new Promise((resolve, reject) => {
            this.getWorkbookParameters()
                .then(res => resolve(res))
                .catch(err => reject(err))
        })

        const foundit = params.filter(p => p.getName() === parameter)

        if(foundit.length === 0) return undefined

        if(foundit.length > 1) {
            let res = []
            foundit.forEach(parameter => {
                const allowableValues = parameter.getAllowableValuesType()
                if(Object.values(TS_ParameterAllowableValuesType).filter(p => p !== 'all').includes(allowableValues)) {
                    // list, range types
                    res.push({ name: parameter.getName(), allowableValues: parameter.getAllowableValues() })
                } else{
                    // all types
                    res.push({ name: parameter.getName(), allowableValues } )
                }
            })

            return foundit
        }

        let res = foundit[0]

        const allowableValues = res.getAllowableValuesType()
        if(allowableValues === 'all') return allowableValues
        if(allowableValues === 'list') return res.getAllowableValues()
    }

    async getWorkBookParametersByName(parameter) {
        if(!this.viz) return []
        const params = await this.getWorkbookParameters()
        const foundit = params.filter(p => p.getName() === parameter)

        return foundit
    }

    // siempre retorna 1 valor, si hay más de uno retorna el primero encontrado
    async getWorkBookParameterByName(parameter) {
        if(!this.viz) return undefined
        const foundit = await this.getWorkBookParametersByName(parameter)

        if(foundit.length === 0) return undefined
        if(foundit.length >= 1) return foundit[0]
        /**
         * Parameter props:
         * getName()
         * getCurrentValue()
         * getDataType()
         * getAllowableValues()
         * getAllowableValuesType()
         * getMaxValue()
         * getMinValue()
         * getStepSize()
         * getDateStepPeriod()
         */
    }

    async getWorkbookParameters({ forceUpdate = false } = { }) {
        if(!this.viz) return []

        return new Promise(async (resolve, reject) => {
            this.getWorkbook().getParametersAsync()
                    .then(res => resolve(res))
                    .otherwise(err => reject(err))
        })
    }

    getWorkSheet({ worksheetName = undefined, activateSheetAsync = true } = {}) {
        if(!activateSheetAsync) {
            let worksheet_found = null
            worksheet_found = this.getActiveSheet({ activateSheetAsync: false })
                                    .getWorksheets()
                                    .filter((worksheet) => worksheet.getName() === worksheetName)

            if(worksheet_found) return worksheet_found[0]
        }

        return this.getActiveSheet({ activateSheetAsync: true, worksheetName })
    }

    async getFilters({ worksheetName = "", activateSheetAsync = false } = {}) {
        let worksheet = null
        return new Promise(async (resolve, reject) => {
            if(worksheetName) {
                if(!activateSheetAsync) {
                    worksheet = this.getWorkSheet({ worksheetName, activateSheetAsync: false })
                } else {
                    worksheet = await this.getWorkSheet({ worksheetName, activateSheetAsync: true })
                        .otherwise((error) => {
                            TSLogger.log(TSViz, error, { error: true })
                        })
                }
            } else {
                worksheet = this.getWorkbook().getActiveSheet()
            }

            if(!worksheet) return null
            if ("getFiltersAsync" in worksheet) {
                worksheet.getFiltersAsync().then(res => resolve(res)).otherwise(err => reject(err))
            }
        })
    }

    async getFiltersByName(filterName, { worksheetName = "", activateSheetAsync = false } = {}) {
        const filters = await this.getFilters({ worksheetName, activateSheetAsync })
        const foundit = filters.filter(filter => filter.getFieldName() === filterName)

        return foundit
    }

    // siempre retorna 1 valor, si hay más de uno retorna el primero encontrado
    async getFilterByName(filterName, { worksheetName = "", activateSheetAsync = false } = {}) {
        const filters = await this.getFiltersByName(filterName, { worksheetName, activateSheetAsync })
        const foundit = filters.filter(filter => filter.getFieldName() === filterName)

        if(foundit.length === 0) return undefined
        if(foundit.length >= 1) return foundit[0]
    }

    async getFilterValueByName(filterName, { worksheetName = "", activateSheetAsync = false, getField = "value" } = {}) {
        const filter = await this.getFilterByName(filterName, { worksheetName, activateSheetAsync })
        if(!filter) return undefined

        const _value = filter.getAppliedValues()
        let value = undefined
        /**
         * value = { value , formattedValue }[]
         */
        if(Array.isArray(_value)) {
            value = _value[_value.length - 1]
            if(!value || !(getField in value)) return value
            return value[getField]
        }

        if(!value || !(getField in value)) return value
        return value[getField]
    }

    _setFilter(filter, { consider_screen = false } = {}) {
        // function para verificar si solo mobile verificará la resolución sino retornara que si se aplicara
        const must_apply_filter = () => consider_screen ? screen.width <= this._mobile_width : true

        // si existe worksheet y se debe aplicar el filtro
        if(this.current_worksheet && must_apply_filter()) {
            const { name, value, type } = filter
            // let valor = "2.4 D ácido 30% ME Dedalo Elite" // debug

            TSLogger.log(TSViz, `filtro[${name}]: "${value}" -> worksheet: "${this.current_worksheet.getName()}"`)
            this.current_worksheet
                .applyFilterAsync(name, value, type)
                .then(res => {
                    TSLogger.log(TSViz, {filter_status: "applyed", filtered_by: { name, value, type } })
                })
                .otherwise(error => {
                    TSLogger.log(TSViz, error, { error: true })
                })
        }
    }

    _applyFilter(filter, worksheetName = undefined, { activateSheetAsync = true, consider_screen = false, forceToGetWorkSheet = false } = {} ) {
        TSFilter.verify(filter, { from: "viz" }) // verifico filtro

        if(!activateSheetAsync) {
            if(!this.current_worksheet || forceToGetWorkSheet) {
                this.current_worksheet = this.getWorkSheet({ worksheetName, activateSheetAsync: false })
            }

            this._setFilter(filter, { consider_screen })
            return true
        }

        if(!this.current_worksheet || forceToGetWorkSheet) {
            this.getWorkSheet({ worksheetName, activateSheetAsync: true })
            .then((worksheet) => {
                this.current_worksheet = worksheet
                this._setFilter(filter, { consider_screen })
            })
            .otherwise((error) => {
                TSLogger.log(TSViz, error, { error: true })
            })

            return true
        }

        this._setFilter(filter, { consider_screen })
        return true
    }

    applyFilter(filter, { activateSheetAsync = false, consider_screen = false } = {}) {
        const { worksheet: data } = filter

        if(typeof(data) === "string") {
            this._applyFilter(filter, data, { activateSheetAsync, consider_screen })
        }

        if(Array.isArray(data)) {
            data.map(worksheet => {
                this._applyFilter(filter, worksheet, { activateSheetAsync, consider_screen })
            })
        }
    }
}

class TSParameter {
    version = "1.0.0"
    vizId = null
    name = ""
    _value = ""
    static dataTypes = TS_ParameterDataType
    dataType = TSParameter.dataTypes.STRING

    static verify({ name, value, vizId , dataType }, { from = "parameter"} = {}) {
        ["parameter","viz"].indexOf(from) === -1 && TSLogger.error(TSParameter, "from param is not valid")
        // if(vizId && typeof(vizId) !== "string") TSLogger.error(TSParameter, "vizId must be string")

        if(
            !Object.values(TSParameter.dataTypes).includes(dataType)
        ) TSLogger.error(TSParameter, `Parameter dataType must be someone of list: ${Object.keys(TSParameter.dataTypes).join(", ")}`)
    }

    constructor({ name, vizId, value = undefined, dataType = TSParameter.dataTypes.STRING }) {
        // verify parameter
        TSParameter.verify({ name , value, vizId, dataType })

        this.name = name
        if(value) this._value = value
        this.dataType = dataType

        this.vizId = vizId
    }

    setVizId(vizId) {
        if(vizId) TSLogger.log(TSParameter, `vizId property already defined, current: ${this.vizId}, received: ${vizId}`, { warning: true })
        this.vizId = vizId
    }

    updateVizId(vizId) {
        // si no tiene vizId lo defino
        if(!vizId) {
            this.setVizId(vizId)
            return
        }
        // si tiene definido, lo actualizo
        this.vizId = vizId
    }

    updateValue(newValue) {
        this._value = newValue
    }

    set value(newValue) {
        this._value = newValue
    }

    get value() {
        return this._value
    }
}

class TSFilter {
    version = "1.0.2"
    worksheet = null
    activeWorkSheet = false
    vizId = null
    name = ""
    _value = ""
    static types = TS_FilterType
    type = TSFilter.types.REPLACE

    static verify({ name, worksheet: worksheet_value, activeWorkSheet, vizId , type }, { from = "filter"} = {}) {
        ["filter","viz"].indexOf(from) === -1 && TSLogger.error(TSFilter, "from param is not valid")
        // if(vizId && typeof(vizId) !== "string") TSLogger.error(TSFilter, "vizId must be string")
        if(!activeWorkSheet && !worksheet_value) TSLogger.error(TSFilter, "worksheet is required")
        // not null|undefined, not string, not array
        if(
            !activeWorkSheet &&
            worksheet_value &&
            typeof(worksheet_value) !== "string" &&
            !Array.isArray(worksheet_value)
        ) TSLogger.error(TSFilter, "woorksheet must be string or string[]")

        if(
            !Object.values(TSFilter.types).includes(type)
        ) TSLogger.error(TSFilter, `Filter type must be someone of list: ${Object.keys(TSFilter.types).join(", ")}`)
    }

    constructor({ name, worksheet: worksheet_value, activeWorkSheet, value, vizId = undefined, type = TSFilter.types.REPLACE }) {
        // verify filter
        TSFilter.verify({ name , worksheet: worksheet_value, activeWorkSheet, vizId, type })

        this.name = name
        if(value) this._value = value
        this.type = type

        this.vizId = vizId ?? null
        this.worksheet = worksheet_value
        this.activeWorkSheet = activeWorkSheet
    }

    setVizId(vizId) {
        if(vizId) TSLogger.log(TSFilter, `vizId property already defined, current: ${this.vizId}, received: ${vizId}`, { warning: true })
        this.vizId = vizId
    }

    updateVizId(vizId) {
        // si no tiene vizId lo defino
        if(!vizId) {
            this.setVizId(vizId)
            return
        }
        // si tiene definido, lo actualizo
        this.vizId = vizId
    }

    updateValue(newValue) {
        this._value = newValue
    }

    set value(newValue) {
        this._value = newValue
    }

    get value() {
        return this._value
    }
}

class TSVizManager {
    version = "1.0.2"
    vizInstances = {}
    filters = {}
    parameters = {}

    /** Parameter Section - start */
    async addParameter(vizId, parameter, { withVizId = true, syncCurrentValue = true } = {}) {
        if(!vizId && withVizId) TSLogger.error(TSParameter, `vizInstance is required`)
        const viz = this.getVizInstance(vizId)
        const get_param = new Promise(async (resolve, reject) => {
            viz.getWorkBookParameterByName(parameter.name)
            .then((res) => { resolve(res)})
            .catch(err => reject(err))
        })

        const param = await get_param
        let default_parameter = { vizId: withVizId ? vizId : null }
        if(syncCurrentValue) {
            const curr_value = param.getCurrentValue()
            // console.log({ from:"viz", parameter, value: curr_value })

            let value
            if(param.getDataType() === TS_ParameterDataType.DATE) {
                const { formattedValue } = curr_value
                value = formattedValue
            } else {
                const { value: syncValue } = curr_value
                value = syncValue
            }
            default_parameter = { ...default_parameter, value }
        }
        const filled_parameter = Object.assign(default_parameter, parameter)

        parameter = new TSParameter(filled_parameter)
        this.parameters[parameter.name] = parameter

        return true
    }

    setVizIdToParameter(parameterName, vizId) {
        if(!vizId) TSLogger.error(TSParameter, `vizInstance is required`)
        if(!(parameterName in this.parameters)) TSLogger.error(TSParameter, `parameter ${parameterName} not in parameters`)

        this.parameters[parameterName].setVizId(vizId)
    }

    getParameter(parameterName) {
        if(!parameterName) TSLogger.error(TSVizManager, `parameterName must be a valid string`)
        if(!(parameterName in this.parameters)) TSLogger.error(TSVizManager, `parameter: ${parameterName} not found`)

        return this.parameters[parameterName]
    }

    async getAllowableValuesOfParameterFromViz(parameterName) {
        if(!parameterName) TSLogger.error(TSVizManager, `parameterName must be a valid string`)
        if(!(parameterName in this.parameters)) TSLogger.error(TSVizManager, `parameter: ${parameterName} not found`)

        const parameter = this.parameters[parameterName]
        if(!(parameter.vizId in this.vizInstances)) TSLogger.error(TSParameter, `vizInstance id: ${parameter.vizId} not found`)

        const vizInstance = this.vizInstances[parameter.vizId]
        return await vizInstance.getWorkBookAllowableValuesOfParameterByParameterName(parameterName)
    }

    async getParameterFromViz(parameterName) {
        if(!parameterName) TSLogger.error(TSVizManager, `parameterName must be a valid string`)
        if(!(parameterName in this.parameters)) TSLogger.error(TSVizManager, `parameter: ${parameterName} not found`)

        const parameter = this.parameters[parameterName]
        if(!(parameter.vizId in this.vizInstances)) TSLogger.error(TSParameter, `vizInstance id: ${parameter.vizId} not found`)

        const vizInstance = this.vizInstances[parameter.vizId]
        new Promise(async (resolve, reject) => {
            const res = await vizInstance.getWorkBookParameterByName(parameterName)
            resolve(res)
        })
    }

    async syncParameter(parameterName) {
        if(!parameterName) TSLogger.error(TSVizManager, `parameterName must be a valid string`)
        if(!(parameterName in this.parameters)) TSLogger.error(TSVizManager, `parameter: ${parameterName} not found`)

        const parameter = this.parameters[parameterName]
        if(!(parameter.vizId in this.vizInstances)) TSLogger.error(TSParameter, `vizInstance id: ${parameter.vizId} not found`)

        const vizInstance = this.vizInstances[parameter.vizId]
        const param = await vizInstance.getWorkBookParameterByName(parameterName)

        if(!param) return
        const curr_value = param.getCurrentValue()
        if(param.getDataType() ===  TS_ParameterDataType.DATE) {
        } else {
            curr_value = param.getCurrentValue()
        }
        // console.log({ from:"viz", parameter, value: curr_value })

        let syncValue
        if(param.getDataType() === TS_ParameterDataType.DATE) {
            const { formattedValue } = curr_value
            syncValue = formattedValue
        } else {
            const { value } = curr_value
            syncValue = value
        }
        this.parameters[parameterName].updateValue(syncValue)
    }

    async updateParameter(parameterName, newValue, { applyToVizInstance = true } = {}) {
        if(!(parameterName in this.parameters)) TSLogger.error(TSParameter, `parameter ${parameterName} not in parameters`)

        const parameter = this.parameters[parameterName]
        parameter.updateValue(newValue)

        if(applyToVizInstance) {
            if(!(parameter.vizId in this.vizInstances)) TSLogger.error(TSParameter, `vizInstance id: ${parameter.vizId} not found`)

            const vizInstance = this.vizInstances[parameter.vizId]
            return await vizInstance.applyParameter(parameter)
        }
    }

    applyParameter(parameterName) {
        if(!(parameterName in this.parameters)) TSLogger.error(TSParameter, `parameter ${parameterName} not in parameters`)

        const parameter = this.parameters[parameterName]
        if(!(parameter.vizId in this.vizInstances)) TSLogger.error(TSParameter, `vizInstance id: ${parameter.vizId} not found`)

        const vizInstance = this.vizInstances[parameter.vizId]
        return vizInstance.applyParameter(parameter)
    }

    removeParameter(name) {
        if(name in this.parameters) delete this.parameters[name]

        return { parameter: name }
    }

    removeAllParameters() {
        const total = Object.keys(this.parameters).length

        this.parameters = {}
        return total
    }
    /** Parameter Section - end */

    /** Filter Section - start */
    async addFilter(vizId, filter, { withVizId = true, syncCurrentValue = true } = {}) {
        if(!vizId && withVizId) TSLogger.error(TSFilter, `vizInstance is required`)

        const filterParams = Object.assign({
            vizId: withVizId ? vizId : null,
            activeWorkSheet: !("worksheet" in filter) ?
                                ("activeWorkSheet" in filter) ?
                                    filter.activeWorkSheet
                                    : true
                                : false
        }, filter)

        filter = new TSFilter(filterParams)

        if(syncCurrentValue) {
            if(!(filter.vizId in this.vizInstances)) TSLogger.error(TSParameter, `vizInstance id: ${filter.vizId} not found`)

            const vizInstance = this.vizInstances[filter.vizId]
            const filter_value = await vizInstance.getFilterValueByName(filter.name, ("worksheet" in filter) && { worksheetName: filter.worksheet })
            if(filter_value) filter.updateValue(filter_value)
        }

        this.filters[filter.name] = filter
    }

    getFilter(filterName) {
        if(!filterName) TSLogger.error(TSVizManager, `filterName must be a valid string`)
        if(!(filterName in this.filters)) TSLogger.error(TSVizManager, `filter: ${filterName} not found`)

        return this.filters[filterName]
    }

    async getFilterFromViz(filterName) {
        if(!filterName) TSLogger.error(TSVizManager, `filterName must be a valid string`)
        if(!(filterName in this.filters)) TSLogger.error(TSVizManager, `filter: ${filterName} not found`)

        const filter = this.filters[filterName]
        if(!(filter.vizId in this.vizInstances)) TSLogger.error(TSVizManager, `vizInstance id: ${filter.vizId} not found`)

        const vizInstance = this.vizInstances[filter.vizId]
        return await vizInstance.getFilterValueByName(filterName)
    }

    async syncFilter(filterName) {
        if(!filterName) TSLogger.error(TSVizManager, `filterName must be a valid string`)
        if(!(filterName in this.filters)) TSLogger.error(TSVizManager, `filter: ${filterName} not found`)

        const filter = this.filters[filterName]
        if(!(filter.vizId in this.vizInstances)) TSLogger.error(TSVizManager, `vizInstance id: ${filter.vizId} not found`)

        const vizInstance = this.vizInstances[filter.vizId]
        const filter_value = await vizInstance.getFilterValueByName(filterName)
        // console.log({ sync: true, filter_value })
        if(!filter_value) return filter_value

        this.filters[filterName].updateValue(filter_value)
        return filter_value
    }

    setVizIdToFilter(filterName, vizId) {
        if(!vizId) TSLogger.error(TSFilter, `vizInstance is required`)
        if(!(filterName in this.filters)) TSLogger.error(TSFilter, `filter ${filterName} not in filters`)

        this.filters[filterName].setVizId(vizId)
    }

    updateFilter(filterName, newValue, { applyToVizInstance = true } = {}) {
        if(!(filterName in this.filters)) TSLogger.error(TSFilter, `filter ${filterName} not in filters`)

        const filter = this.filters[filterName]
        filter.updateValue(newValue)

        if(applyToVizInstance) {
            if(!(filter.vizId in this.vizInstances)) TSLogger.error(TSFilter, `vizInstance id: ${filter.vizId} not found`)

            const vizInstance = this.vizInstances[filter.vizId]
            vizInstance.applyFilter(filter)
        }
    }

    applyFilter(filterName) {
        if(!(filterName in this.filters)) TSLogger.error(TSFilter, `filter ${filterName} not in filters`)

        const filter = this.filters[filterName]
        if(!(filter.vizId in this.vizInstances)) TSLogger.error(TSFilter, `vizInstance id: ${filter.vizId} not found`)

        const vizInstance = this.vizInstances[filter.vizId]
        vizInstance.applyFilter(filter)
    }

    removeFilter(name) {
        if(name in this.filters) delete this.filters[name]

        return { filter: name }
    }

    removeAllFilters() {
        const total = Object.keys(this.filters).length

        this.filters = {}
        return total
    }
    /** Filter Section - end */

    /** VizInstance Section - start */
    addVizInstances(vizOpts) {
        const instance = new TSViz(vizOpts)
        this.vizInstances[instance.id] = instance
    }

    getVizInstance(vizId) {
        if(!vizId) TSLogger.error(TSVizManager, `vizId must be a valid number`)
        if(!(vizId in this.vizInstances)) TSLogger.error(TSVizManager, `vizInstance id: ${vizId} not found`)

        return this.vizInstances[vizId]
    }

    removeVizInstance(vizId) {
        delete this.vizInstances[vizId]

        return { vizId }
    }

    removeAllVizInstance() {
        const total = Object.keys(this.vizInstances).length

        this.vizInstances = {}
        return total
    }
    /** VizInstance Section - end */
}
